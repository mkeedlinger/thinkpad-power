use crate::args::cpufreq::{CpuFreq, CpuFreqSubcommand};
use crate::prelude::*;
use crate::utils::{get_upper_cpu, BASE_CPU_PATH};
use std::fs::{read_to_string, write};
use std::path::PathBuf;

pub fn cpufreq(args: CpuFreqSubcommand) -> Result<()> {
    let upper = get_upper_cpu()?;

    match args {
        CpuFreqSubcommand::Get => {
            const CPU_NAME_WIDTH: usize = 10;
            const CPU_CUR_WIDTH: usize = 15;
            const CPU_MAX_WIDTH: usize = 20;

            println!(
                "{:<CPU_NAME_WIDTH$}{:>CPU_CUR_WIDTH$}{:>CPU_MAX_WIDTH$}",
                "CPU", "Current", "Max"
            );

            for cpu in 0..=upper {
                let mut path = PathBuf::from(BASE_CPU_PATH);
                path.push(format!("cpu{}", cpu));
                path.push("cpufreq");
                path.push("scaling_cur_freq");

                let cur_freq: usize = read_to_string(path)?.trim().parse()?;

                let mut path = PathBuf::from(BASE_CPU_PATH);
                path.push(format!("cpu{}", cpu));
                path.push("cpufreq");
                path.push("scaling_max_freq");

                let max_freq: usize = read_to_string(path)?.trim().parse()?;

                println!(
                    "{:<CPU_NAME_WIDTH$}{:>CPU_CUR_WIDTH$}{:>CPU_MAX_WIDTH$}",
                    format!("cpu{}:", cpu),
                    format!("{} MHz", cur_freq / 1000),
                    format!("{} MHz", max_freq / 1000)
                );
            }
        }
        CpuFreqSubcommand::Set(freq) => match freq {
            CpuFreq::Default => {
                for cpu in 0..=upper {
                    let mut info_path = PathBuf::from(BASE_CPU_PATH);

                    info_path.push(format!("cpu{}", cpu));
                    info_path.push("cpufreq");
                    info_path.push("cpuinfo_max_freq");

                    let cpu_max = read_to_string(info_path)?;

                    let mut scaling_max_path = PathBuf::from(BASE_CPU_PATH);

                    scaling_max_path.push(format!("cpu{}", cpu));
                    scaling_max_path.push("cpufreq");
                    scaling_max_path.push("scaling_max_freq");

                    write(scaling_max_path, cpu_max)?;
                }
            }
            CpuFreq::Value { mhz_hundreds: mhz } => {
                for cpu in 0..=upper {
                    let mut scaling_max_path = PathBuf::from(BASE_CPU_PATH);

                    scaling_max_path.push(format!("cpu{}", cpu));
                    scaling_max_path.push("cpufreq");
                    scaling_max_path.push("scaling_max_freq");

                    let khz = (mhz as usize) * 100_000;

                    write(scaling_max_path, khz.to_string())?;
                }
            }
        },
    }

    Ok(())
}
