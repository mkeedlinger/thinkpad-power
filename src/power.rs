use crate::args::power::{PowerProfile, PowerProfileSubcommand};
use crate::prelude::*;
use crate::utils::{get_upper_cpu, BASE_CPU_PATH};
use std::fs::{read_to_string, write};
use std::path::PathBuf;

pub fn power(sub: PowerProfileSubcommand) -> Result<()> {
    let upper = get_upper_cpu()?;

    match sub {
        PowerProfileSubcommand::Get => {
            const CPU_NAME_WIDTH: usize = 10;
            const CPU_PROFILE_WIDTH: usize = 25;

            println!(
                "{:<CPU_NAME_WIDTH$}{:<CPU_PROFILE_WIDTH$}",
                "CPU", "Profile",
            );

            for cpu in 0..=upper {
                let mut path = PathBuf::from(BASE_CPU_PATH);
                path.push(format!("cpu{}", cpu));
                path.push("cpufreq");
                path.push("energy_performance_preference");

                let profile = read_to_string(path)?;
                let profile = profile.trim();

                println!(
                    "{:<CPU_NAME_WIDTH$}{:<CPU_PROFILE_WIDTH$}",
                    format!("cpu{}:", cpu),
                    profile,
                );
            }
        }
        PowerProfileSubcommand::Set(profile) => match profile {
            PowerProfile::Default => todo!(),
            PowerProfile::Value { profile } => {
                let profile = format!("{:?}", &profile);
                let profile = profile.to_lowercase();

                for cpu in 0..=upper {
                    let mut path = PathBuf::from(BASE_CPU_PATH);

                    path.push(format!("cpu{}", cpu));
                    path.push("cpufreq");
                    path.push("energy_performance_preference");

                    write(path, &profile)?;
                }
            }
        },
    }

    Ok(())
}
