use clap::Subcommand;

#[derive(Debug, Subcommand)]
pub enum FanSubcommand {
    Get,

    #[clap(subcommand)]
    Set(FanSpeeds),
}

#[derive(Subcommand, Debug)]
pub enum FanSpeeds {
    Auto,
    Max,
}
