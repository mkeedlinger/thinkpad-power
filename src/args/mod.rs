use self::cpufreq::CpuFreqSubcommand;
use self::fan::FanSubcommand;
use self::power::PowerProfileSubcommand;
use clap::Parser;

pub mod cpufreq;
pub mod fan;
pub mod power;

#[derive(Parser, Debug)]
pub enum Cli {
    #[clap(subcommand)]
    Fan(FanSubcommand),

    #[clap(subcommand)]
    Freq(CpuFreqSubcommand),

    #[clap(subcommand)]
    Power(PowerProfileSubcommand),
}
