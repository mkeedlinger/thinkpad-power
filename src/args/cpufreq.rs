use clap::Subcommand;

#[derive(Debug, Subcommand)]
pub enum CpuFreqSubcommand {
    Get,

    #[clap(subcommand)]
    Set(CpuFreq),
}

#[derive(Subcommand, Debug)]
pub enum CpuFreq {
    Default,

    Value { mhz_hundreds: u16 },
}
