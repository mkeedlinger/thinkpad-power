use crate::args::fan::FanSpeeds;
use crate::args::fan::FanSubcommand;
use crate::prelude::*;
use once_cell::sync::Lazy;
use regex::Regex;
use std::fs::{read_to_string, write};

const FAN_PATH: &str = "/proc/acpi/ibm/fan";

static OUTPUT_MATCHER: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        r"(?x)
        status: \s+ (?P<status>(enabled|disabled)) \n
        speed: \s+ (?P<speed> \S+) \n
        level: \s+ (?P<level> \S+) \n
        ",
    )
    .unwrap()
});

pub fn fan(args: FanSubcommand) -> Result<()> {
    match args {
        FanSubcommand::Get => {
            let output = read_to_string(FAN_PATH)?;
            let matches = OUTPUT_MATCHER
                .captures(&output)
                .ok_or_else(|| anyhow!("System output failed to match regex"))?;

            println!(
                "status: {}\nspeed: {}\nlevel: {}",
                matches.name("status").unwrap().as_str(),
                matches.name("speed").unwrap().as_str(),
                matches.name("level").unwrap().as_str()
            );
        }
        FanSubcommand::Set(speed) => match speed {
            FanSpeeds::Auto => {
                write(FAN_PATH, "level auto").context("Could not write to system file")?
            }
            FanSpeeds::Max => {
                write(FAN_PATH, "level 7").context("Could not write to system file")?
            }
        },
    }

    Ok(())
}
