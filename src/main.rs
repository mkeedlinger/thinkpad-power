use crate::args::Cli;
use crate::fan::fan;
use crate::power::power;
use crate::prelude::*;
use clap::Parser;
use cpufreq::cpufreq;

mod args;
mod cpufreq;
mod fan;
mod power;
mod prelude;
mod utils;

fn main() -> Result<()> {
    match Cli::parse() {
        Cli::Fan(sub) => fan(sub)?,
        Cli::Freq(args) => cpufreq(args)?,
        Cli::Power(sub) => power(sub)?,
    }

    Ok(())
}
