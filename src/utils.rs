use crate::prelude::*;
use once_cell::sync::Lazy;
use regex::Regex;
use std::fs::read_to_string;

const PRESENT_CPU_PATH: &str = "/sys/devices/system/cpu/present";
pub const BASE_CPU_PATH: &str = "/sys/devices/system/cpu";

static CPU_DIR_REGEX: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        r"(?x)
        \d+
        -
        (?P<upper> \d+) 
        ",
    )
    .unwrap()
});

pub fn get_upper_cpu() -> Result<u8> {
    let present_cpus = read_to_string(PRESENT_CPU_PATH)?;
    let cap = CPU_DIR_REGEX
        .captures(&present_cpus)
        .ok_or_else(|| anyhow!("Expected system cpu file to match regex"))?;

    let upper = cap.name("upper").unwrap().as_str();
    let upper: u8 = upper.parse()?;

    Ok(upper)
}
