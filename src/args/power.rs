use clap::Subcommand;
use phf::phf_map;
use std::str::FromStr;

static PROFILES: phf::Map<&'static str, Profile> = phf_map! {
    "performance" => Profile::Performance,
    "balance-performance" => Profile::BalancePerformance,
    "balance-power" => Profile::BalancePower,
    "power" => Profile::Power,
};

#[derive(Debug, Subcommand)]
pub enum PowerProfileSubcommand {
    Get,

    #[clap(subcommand)]
    Set(PowerProfile),
}

#[derive(Subcommand, Debug)]
pub enum PowerProfile {
    Default,

    Value { profile: Profile },
}

#[derive(Debug, Clone, Copy)]
pub enum Profile {
    Performance,
    BalancePerformance,
    BalancePower,
    Power,
}

impl FromStr for Profile {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let profile = PROFILES.get(s);
        match profile {
            Some(profile) => Ok(*profile),
            None => {
                let mut choices = vec![];

                for (choice, _) in PROFILES.into_iter() {
                    choices.push(choice.to_owned());
                }

                let choices = choices.join(", ");

                Err(ParseError {
                    source_string: s.to_owned(),
                    expects: choices,
                })
            }
        }
    }
}

#[derive(thiserror::Error, Debug)]
#[error("Could not parse `{source_string}`. Expects one of {expects}.")]
pub struct ParseError {
    source_string: String,
    expects: String,
}
